const arr = ['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e']

function groupInObj(arr) {
    let result = []
    let str = ''
    for (let i = 0; i < arr.length; i++) {
        str += arr[i]
        if (i < arr.length - 1 && arr[i] !== arr[i + 1]) {
            str += ' '
        }
    }
    let temp = str.split(' ')
    for (let i = 0; i < temp.length; i++) {
        result.push({ [temp[i].length]: temp[i][0] })
    }
    console.log(result);
}

function groupInArr(arr) {
    let result = []
    let str = ''
    for (let i = 0; i < arr.length; i++) {
        str += arr[i]
        if (i < arr.length - 1 && arr[i] !== arr[i + 1]) {
            str += ' '
        }
    }
    let temp = str.split(' ')
    for (let i = 0; i < temp.length; i++) {
        result.push([temp[i].length, temp[i][0]])
    }
    console.log(result);
}


groupInObj(arr) // [ {3, 'a'}, {1, 'b'}, {2, 'c'}, {3, 'b'}, {2, 'd'}, {3, 'e'} ] 
groupInArr(arr) // [ [3, 'a'], [1, 'b'], [2, 'c'], [3, 'b'], [2, 'd'], [3, 'e'] ] 
