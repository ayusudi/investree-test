const arr = ['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e']

function groupMultiArr(arr) {
    let result = []
    let str = ''
    for (let i = 0; i < arr.length; i++) {
        str += arr[i]
        if (i < arr.length - 1 && arr[i] !== arr[i + 1]) {
            str += ' '
        }
    }
    let temp = str.split(' ')
    for (let i = 0; i < temp.length; i++) {
        result.push(temp[i].split(''))
    }
    console.log(result);
}

groupMultiArr(arr) // [ [‘a’, ‘a’, ‘a’], [‘b’], [‘c’, ‘c’], [‘b’, ‘b’, ‘b’], [‘d’, ‘d’], [‘e’, ‘e’, ‘e’] ]
