import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from './views/Dashboard.vue';
import DetailSub from './views/DetailSub.vue';
import Reksadana from './views/Reksadana.vue';
import Sbn from './views/Sbn.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
    },
    {
      path: '/invoice/conventional',
      name: 'invoiceConv',
      component: DetailSub,
    },
    {
      path: '/invoice/sharia',
      name: 'invoiceSharia',
      component: DetailSub,
    },
    {
      path: '/osf/conventional',
      name: 'osfConv',
      component: DetailSub,
    },
    {
      path: '/osf/sharia',
      name: 'osfSharia',
      component: DetailSub,
    },
    {
      path: '/sbn',
      name: 'sbn',
      component: Sbn,
    },
    {
      path: '/reksadana',
      name: 'reksadana',
      component: Reksadana,
    },
    {
      path: '/*',
      name: 'redirect',
      beforeEnter: (to, from, next) => {
        next({ path: '/' });
      },
    },
  ],
});
