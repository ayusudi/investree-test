# **Investree Test**

# **Logic**
All simple logic test are inside Logic folder.  

# **Client**
## **Getting Started**
1. cd Web-App
2. npm install
3. npm run serve

## **Fitur**
1. Search
2. Filter

## **Views**
### **Dashboard**
![Screenshot](./Screenshot/1.png)

### **SubType**
![Screenshot](./Screenshot/2.png)

### **Detail**
![Screenshot](./Screenshot/3.png)