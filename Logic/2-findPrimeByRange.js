function isPrime(num) {
    for (let i = 2; i < num; i++) {
        if (num % i === 0) {
            return false
        }
    }
    return true
}

function findPrimeByRange(min, max) {
    let result = []
    for (let i = min; i < max; i++) {
        if (isPrime(i)) {
            result.push(i)
        }
    }
    console.log(result);
}


findPrimeByRange(11, 40) // [11, 13, 17, 19, 23, 29, 31, 37]
